﻿doctestprinter.strip\_trailing\_tabs
====================================

.. currentmodule:: doctestprinter

.. autofunction:: strip_trailing_tabs