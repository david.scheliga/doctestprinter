﻿doctestprinter.strip\_trailing\_whitespaces
===========================================

.. currentmodule:: doctestprinter

.. autofunction:: strip_trailing_whitespaces