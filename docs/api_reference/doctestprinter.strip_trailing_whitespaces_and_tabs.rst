﻿doctestprinter.strip\_trailing\_whitespaces\_and\_tabs
======================================================

.. currentmodule:: doctestprinter

.. autofunction:: strip_trailing_whitespaces_and_tabs