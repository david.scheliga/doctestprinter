﻿doctestprinter.remove\_trailing\_whitespaces\_and\_tabs
=======================================================

.. currentmodule:: doctestprinter

.. autofunction:: remove_trailing_whitespaces_and_tabs