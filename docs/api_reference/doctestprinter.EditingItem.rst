﻿doctestprinter.EditingItem
==========================

.. currentmodule:: doctestprinter

.. autoclass:: EditingItem

   
   .. automethod:: __init__

   
   .. rubric:: Methods

   .. autosummary::
   
      ~EditingItem.__init__
   
   

   
   
   