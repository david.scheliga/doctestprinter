﻿doctestprinter.remove\_trailing\_whitespaces
============================================

.. currentmodule:: doctestprinter

.. autofunction:: remove_trailing_whitespaces